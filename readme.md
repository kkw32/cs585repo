#What's in here
The repo for CS 585 Game Development (SIT).

#How to build
To build the unit test of all data structures:

    $ make utest
To build the game;

    $ make gtest
To build the unit test for JSON parser:

    $ make jtest
then run the executable to start:

    $ ./*test
All unit test and simluation logs are currently logged to files. Plain text are
used for all logs, you can trace what's going on by:

    $ tail -f log_*
Or wait till the test is end, then print them out:

    $ cat log_*
to clean builds:

    $ make cl