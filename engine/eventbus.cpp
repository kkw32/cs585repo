#include <engine/eventbus.h>

gqueue<event *>            eventbus::events;
ghashmap<ghashmap<void *>> eventbus::ltable;

void eventbus::dispatch(event *e) {

  string type=e->get_type();

  if (!ltable.count(type)) {
    return;
  }

  ghashmap<void *> &handlers=ltable[type];
  garray<string> handler_keys=handlers.keys();

  for (int i=0; i<handler_keys.size(); i++) {
    static_cast<handler<event *> *>(handlers[handler_keys[i]])->_handle_event(e);
  }

}

void eventbus::post(event *e) {
  events.push(e);
}

void eventbus::run() {
  while (!events.empty()) {
    event *e=events.front();
    events.pop();
    dispatch(e);
    delete e;
  }
}

void eventbus::shutdown() {
  while (!events.empty()) {
    event *e=events.front();
    delete e;
    events.pop();
  }
}
