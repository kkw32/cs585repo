/**
 * A simple fixed grid (NxM) scene graph
 *
 * Kai W(kkw)
 */
#ifndef FIXED_GRID_H
#define FIXED_GRID_H

#include <utils/garray.h>
using namespace std;

class scene_node;
class scene_graph;

class fixed_grid : public scene_graph {

protected:

  int pos_x, pos_y;
  int height, width;
  garray<scene_node *> ***map;

  void add_node(int, int, scene_node *);
  scene_node *remove_node(int, int, uintptr_t);

  scene_node *get_node(int, int);
  scene_node *get_node_fromcur(int, int);

  void map_coords(int &, int &);
  bool bound(int, int);
  bool has_node(int, int);

  void do_render();

  garray<scene_node *> get_colliders(scene_node *);

public:

  fixed_grid(int, int);

  int get_height() { return height; }
  int get_width()  { return width; }

  bool move_pos(int, int);
  ~fixed_grid();

};
#endif
