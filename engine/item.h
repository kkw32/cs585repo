/**
 * Game iten
 *
 * Kai W(kkw)
 */
#ifndef ITEM_H
#define ITEM_H

#include <string>
#include <vector>
using namespace std;

class item {
  private:
    string name;

    int level;
    vector<int> price_range;
    vector<int> bonus_range;

  public:
    item(const string &n) : level(0) { name = n; }

    const string &get_name() { return name; }
    int get_level() { return level; }
    int get_price(int l) { return price_range[l]; }
    int get_bonus() { return bonus_range[level]; }

    void set_level(int l) { level = l; }
    void set_price_range(const vector<int> &pr) { price_range = pr; }
    void set_bonus_range(const vector<int> &br) { bonus_range = br; }
};
#endif
