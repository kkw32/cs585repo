#include <cstdarg>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <engine/logger.h>
#include <fcntl.h>
#include <unistd.h>
#include <unordered_set>

/* Logging sources */
unordered_set<string> sources;

const char *mapv[3] = {"[debug", "[unittest", "[release"};
const char *maph[3] = {"warn]", "erro]", "logg]"};

const int logger::DBG_WARN = 0x0000; // high 8 bits: category, low 8 bits: type
const int logger::DBG_ERRO = 0x0001;
const int logger::DBG_LOGG = 0x0002;

const int logger::UNI_WARN = 0x0100;
const int logger::UNI_ERRO = 0x0101;
const int logger::UNI_LOGG = 0x0102;

const int logger::PLY_WARN = 0x0200;
const int logger::PLY_ERRO = 0x0201;
const int logger::PLY_LOGG = 0x0202;

void logger::init() {
    /* Default logging source */
    sources.emplace("log");
}

void logger::add_source(const string& path) { sources.insert(path); }

void logger::remove_source(const string& path) { sources.erase(path); }

void logger::log(const int wht, const char *fmt, ...) {

#ifdef NLOG
    return;
#endif

    time_t t;
    time(&t);
    char *buf = ctime(&t);
    buf[strlen(buf) - 1] = ']';

    int i = 0;
    char buf_log[1024] = "";
    i += sprintf(buf_log, "[%s-%s-%s: ", buf, mapv[(wht & (0xff << 8)) >> 8],
                 maph[wht & 0xff]);
    va_list vl;
    va_start(vl, fmt);
    i += vsprintf(buf_log + i, fmt, vl);
    va_end(vl);
    sprintf(buf_log + i, "\n");

    for (const auto &s : sources) {
        int fd = open(s.c_str(), O_CREAT | O_WRONLY | O_APPEND, 00666);
        write(fd, buf_log, strlen(buf_log));
        close(fd);
    }
}
