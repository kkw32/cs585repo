/**
 * Window manager
 *
 * Kai W(kkw)
 */
#ifndef WINDOW_H
#define WINDOW_H

#include <ncurses.h>

namespace window {

  /**
   * Camera window size
   */
  extern int cam_h, cam_w;
  /**
   * Infomation window size
   */
  extern int inf_h, inf_w;
  /**
   * Camera cursor coords
   */
  extern int cur_x, cur_y;

  extern WINDOW *cam_win;
  extern WINDOW *inf_win;
  /**
   * Sets up game windows, all consts should be put into a JSON (future)
   */
  void init();
  /**
   * Tests if the coords is bound in camera region
   */
  int bound(int, int);
  /**
   * Moves cursor
   */
  bool mov_cur(int, int);

  void show_cur();
  void hide_cur();

};
#endif
