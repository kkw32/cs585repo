/**
 * Actor
 *
 * Kai W(kkw)
 */
#ifndef ACTOR_H
#define ACTOR_H

#include <stack>
#include <string>
using namespace std;

class state;

class actor {

  protected:
    string name;

    stack<state *> states;

    /* Geometry */
    int width, height;
    int x, y;
    int tile_type;
    int tar_x, tar_y;
    int org_x, org_y; // previous coords before going to target

    /* Properties */
    int team;
    int health;
    int health_cur;
    int dmg_min;
    int dmg_max;
    int def_min;
    int def_max;
    int dmg_bonus; // dmg bonus from item.
    int def_bonus; // def bonus from item.
    int att_interval;
    bool dead;
    bool chosen;
    bool in_combat;

    /* Drawable */
    char rep_char;
    int color;

  public:
    /**
     * I - name
     */
    actor(string);
    /**
     * Accessors & Setters
     */
    string get_name() { return name; }

    int get_width() { return width; }
    int get_height() { return height; }
    int get_x() { return x; }
    int get_y() { return y; }
    int get_tarx() { return tar_x; }
    int get_tary() { return tar_y; }
    int get_type() { return tile_type; }

    int get_team() { return team; }
    int get_health() { return health; }
    int get_health_cur() { return health_cur; }
    int get_dmg_min() { return dmg_min; }
    int get_dmg_max() { return dmg_max; }
    int get_def_min() { return def_min; }
    int get_def_max() { return def_max; }
    int get_dmg_bonus() { return dmg_bonus; }
    int get_def_bonus() { return def_bonus; }
    int get_att_interval() { return att_interval; }
    bool is_chosen() { return chosen; }
    bool is_incombat() { return in_combat; }

    void set_name(string _name) { name = _name; }

    void set_width(int w) { width = w; }
    void set_height(int h) { height = h; }
    void set_x(int _x) { x = _x; }
    void set_y(int _y) { y = _y; }
    void set_tarx(int _x) { tar_x = _x; }
    void set_tary(int _y) { tar_y = _y; }
    void set_type(int ttype) { tile_type = ttype; }

    void set_team(int t) { team = t; }
    void set_health(int h) { health = h; }
    void set_health_cur(int h) { health_cur = h; }
    void set_dmg(int l, int r) { dmg_min = l, dmg_max = r; }
    void set_def(int l, int r) { def_min = l, def_max = r; }
    void set_att_interval(int ai) { att_interval = ai; }
    void set_choice(int c) { chosen = c; }
    void set_combat(int c) { in_combat = c; }

    /* I - represent char, color */
    void set_drawable(char rep, int c) { rep_char = rep, color = c; }

    bool is_dead() { return dead; }
    /* I - dmg amount */
    void take_dmg(int);
    /**
     * Updates actor state
     *
     * I - new tick
     */
    void update(int);
    /**
     * Draws the actor on camera-window
     *
     * I - coord-x, coord-y
     */
    void draw(int, int);
    /**
     * O - current actor state (top state)
     */
    state *cur_state();
    /**
     * Pushes a new state into state stack, and makes it current state
     *
     * I - state to push
     */
    void push_state(state *);
    /**
     * Pops current state from state stack
     */
    void pop_state();
    /**
     * Tests if collides with the other actor
     */
    bool collide(actor *);
    /**
     * Draws actor info on info-window
     */
    void dump();

    virtual bool is_player();
    virtual bool is_building();

    virtual ~actor();
};
#endif
