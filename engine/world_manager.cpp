#include <engine/smanager.h>
#include <engine/scene_graph.h>
#include <engine/actor.h>
#include <engine/actor_manager.h>
#include <engine/factory.h>
#include <engine/state.h>
#include <engine/jobject.h>
#include <engine/scene_node.h>
#include <engine/common.h>
#include <engine/state.h>
#include <engine/fixed_grid.h>
#include <engine/world_manager.h>

/**
 * Different shapes in fixed graph
 *
 * key - shape
 * val - create_shape func, I - x, y, height, width/slope
 */
typedef void (*create_shape)(actor *, int, int, int, int);
ghashmap<create_shape> shape_funcs;

/* Rectangle */
void create_rect(actor *a, int x, int y, int h, int w) {
  for (int o=0; o<h; o++)
    for (int p=0; p<w; p++) if (o || p) { // avoid add twice to (x, y) [EVIL]
      smanager::get_graph()->add_node(x+o, y+p, new scene_node(a));
    }
}

/* Triangle 2 */
void create_trg2(actor *a, int x, int y, int h, int w) {
  int stop=y;
  for (int o=0; o<h; o++) {
    for (int p=y; p>=stop; p--) if (o || p!=y) {
      smanager::get_graph()->add_node(x+o, p, new scene_node(a));
    }
    stop-=w;
  }
}

/* Triangle 3 */
void create_trg3(actor *a, int x, int y, int h, int w) {
  for (int o=0; o<h; o++)
    for (int p=h-1-o; p>=0; p--) if (o || p) {
      smanager::get_graph()->add_node(x+o, y+p, new scene_node(a));
    }
}

/* Triangle 4 */
void create_trg4(actor *a, int x, int y, int h, int w) {
  int stop=y;
  for (int o=0; o<h; o++) {
    for (int p=y; p>=stop; p--) if (o || p!=y) {
      smanager::get_graph()->add_node(x+o, p, new scene_node(a));
    }
    if (o%4==0) stop--;
  }
}

/* Triangle 5 */
void create_trg5(actor *a, int x, int y, int h, int w) {
  int left=y, right=y;
  for (int o=0; o<h; o++) {
    for (int p=left; p<=right; p++) if (o || p!=y) {
      smanager::get_graph()->add_node(x+o, p, new scene_node(a));
    }
    left-=w;
    right+=w;
  }
}

void world_manager::init() {
  shape_funcs["rect"]=&create_rect;
  shape_funcs["trg2"]=&create_trg2;
  shape_funcs["trg3"]=&create_trg3;
  shape_funcs["trg4"]=&create_trg4;
  shape_funcs["trg5"]=&create_trg5;
}

void world_manager::spawn_mob(string type) {

  int height=smanager::get_graph()->get_height();
  int width=smanager::get_graph()->get_width();
  int x=rand()%height;
  int y=rand()%width;

  /* Find empty cell */
  while (smanager::get_graph()->get_node(x, y)->get_owner()->get_type()!=
    ENGINE::TILE_CAN) {
    x=rand()%height;
    y=rand()%width;
  }

  actor *tick=factory::make<actor *>(type);
  tick->set_x(x);
  tick->set_y(y);
  actor_manager::add_tick(tick);

}

void world_manager::init_scene_graph(int width, int height) {
  smanager::set_graph(new fixed_grid(height, width));
}


void world_manager::init_tiles(jobject &map_data) {

  for (int i=0; i<map_data.size(); i++) {

    jobject data=map_data[i];
    string type=data[0].jstring;
    string shape=data[1].jstring;

    int x=data[2].jint;
    int y=data[3].jint;
    int h=data[4].jint;
    int w=data[5].jint;

    actor *tile=factory::make<actor *>(type);
    tile->set_x(x);
    tile->set_y(y);
    actor_manager::add_actor(tile);

    (*shape_funcs[shape])(tile, x, y, h, w);

  }

}

void world_manager::init_mobs(jobject& mob_data) {
  for (int i=0; i<mob_data.size(); i++) {
    jobject data=mob_data[i];
    for (int j=0; j<data[1].jint; j++) {
      spawn_mob(data[0].jstring);
    }
  }
}
