#include <cctype>
#include <engine/jobject.h>
#include <engine/jparser.h>
#include <fstream>
#include <stdexcept>

bool jparser::expsign(char x) { return x == '-' || x == '+'; }
bool jparser::exp(char x) { return x == 'e' || x == 'E'; }

void jparser::trim_space(int &pos) {
    while (pos < json_strlen && json_str[pos] == ' ') {
        pos++;
    }
}

void jparser::trim_digit(int &pos) {
    while (pos < json_strlen && isdigit(json_str[pos])) {
        pos++;
    }
}

void jparser::basic_string(int &pos, const string &str) {
    int offset = 0;
    while (pos + offset < json_strlen &&
           str[offset] == json_str[pos + offset]) {
        offset++;
    }

    if (offset != str.length()) {
        throw logic_error(
            "jparser::basic_string: invalid JSON boolean or null");
    }

    pos += offset;
}

shared_ptr<jobject> jparser::_parse(int &pos) {
    trim_space(pos);
    /* JSON string doesn't have close symbol ", ], }  */
    if (pos == json_strlen) {
        throw logic_error("jparser::_parse(int&): invalid JSON object");
    }

    /* Basic case JSTRING */
    if (json_str[pos] == '"') {
        int pos_start = pos, offset = 1;
        while (pos + offset < json_strlen && json_str[pos + offset] != '"') {
            offset++;
        }
        if (pos + offset == json_strlen) {
            throw logic_error("jparser::_parse(int&): invalid JSON string");
        }
        pos += offset + 1;
        return make_shared<jobject>(json_str.substr(pos_start + 1, offset - 1));
    } else if (json_str[pos] == 't') { /* Basic case JBOOL true */
        basic_string(pos, string("true"));
        return make_shared<jobject>(true);
    } else if (json_str[pos] == 'f') { /* Basic case JBOOL false */
        basic_string(pos, string("false"));
        return make_shared<jobject>(false);
    } else if (json_str[pos] == 'n') { /* Basic case JNULL */
        basic_string(pos, string("null"));
        return make_shared<jobject>();
    } else if (isdigit(json_str[pos]) ||
               json_str[pos] == '-') { /* Basic case JINT and JDOUBLE */
        /* Sign digit */
        int sign = 1;
        if (json_str[pos] == '-') {
            pos++;
            if (pos == json_strlen || !isdigit(json_str[pos])) {
                throw logic_error("jparser::_parse(int&): invalid JSON int");
            }
            sign = -1;
        }
        /* If there is a leading 0, proceed one step to check . for fraction */
        int pos_start = pos;
        if (json_str[pos] == '0') {
            pos++;
        } else {
            trim_digit(pos);
        }
        /* JINT */
        if (pos == json_strlen ||
            (json_str[pos] != '.' && !exp(json_str[pos]))) {
            long long ret;
            sscanf(json_str.substr(pos_start, pos - pos_start + 1).c_str(),
                   "%lld", &ret);
            return make_shared<jobject>(sign * ret);
        }
        /* JDOUBLE */
        if (json_str[pos] == '.') {
            pos++;
            if (pos == json_strlen || !isdigit(json_str[pos])) {
                throw logic_error("jparser::_parse(int&): invalid JSON double");
            }

            trim_digit(pos);
            /* End of string. Or no eE is found */
            if (pos == json_strlen || !exp(json_str[pos])) {
                double ret;
                sscanf(json_str.substr(pos_start, pos - pos_start + 1).c_str(),
                       "%lf", &ret);
                return make_shared<jobject>(sign * ret);
            }
        }
        /* Symbol other than digits or eE is found */
        pos++;
        if (pos == json_strlen ||
            (!expsign(json_str[pos]) && !isdigit(json_str[pos]))) {
            throw logic_error("jparser::_parse(int&): invalid JSON number");
        }
        /* JDOUBLE with eE expression */
        if (expsign(json_str[pos])) {
            pos++;
            if (pos == json_strlen || !isdigit(json_str[pos])) {
                throw logic_error("jparser::_parse(int&): invalid JSON double");
            }
        }
        trim_digit(pos);
        /* End of string */
        if (pos == json_strlen || !isdigit(json_str[pos])) {
            double ret;
            sscanf(json_str.substr(pos_start, pos - pos_start + 1).c_str(),
                   "%lf", &ret);
            return make_shared<jobject>(sign * ret);
        }
        throw logic_error("jparser::_parse(int&): invalid JSON number");
    } else if (json_str[pos] == '[') { /* JLIST */
        auto ret = make_shared<jobject>(JLIST);

        while (pos < json_strlen) {
            pos++;
            auto element = _parse(pos);

            /* No elements parsed, empty list */
            if (element->get_type() == JEMPTY) {
                return ret;
            }
            ret->add(element);

            trim_space(pos);
            /* String doesn't closes with ] or, , is not found */
            if (pos == json_strlen ||
                (json_str[pos] != ',' && json_str[pos] != ']')) {
                throw logic_error("jparser::_parse(int&): invalid JSON array");
            }
            if (json_str[pos] == ']') {
                pos++;
                break;
            }
        }

        return ret;
    } else if (json_str[pos] == '{') { /* JDICT */
        auto ret = make_shared<jobject>(JDICT);

        while (pos < json_strlen) {
            pos++;
            auto key = _parse(pos);
            /* No keys parsed, empty dict */
            if (key->get_type() == JEMPTY) {
                return ret;
            }
            if (key->get_type() != JSTRING) {
                throw logic_error("jparser::_parse(int&): invalid JSON dict");
            }
            trim_space(pos);
            if (pos == json_strlen || json_str[pos] != ':') {
                throw logic_error("jparser::_parse(int&): invalid JSON dict");
            }

            pos++;
            auto val = _parse(pos);
            (*ret)[key->jstring] = val;

            trim_space(pos);
            if (pos == json_strlen ||
                (json_str[pos] != ',' && json_str[pos] != '}')) {
                throw logic_error("jparser::_parse(int&): invalid JSON dict");
            }
            if (json_str[pos] == '}') {
                pos++;
                break;
            }
        }
        return ret;
    } else if (json_str[pos] == '}' ||
               json_str[pos] == ']') { /* Empty list or dict */
        pos++;
        return make_shared<jobject>(JEMPTY);
    } else {
        throw logic_error("jparser::_parse(int&): invalid JSON object all");
    }
}

shared_ptr<jobject> jparser::_parse(const string &jstr) {
    json_str = jstr;
    json_strlen = jstr.length();
    int start = 0;
    return _parse(start);
}

shared_ptr<jobject> jparser::parse(const string &path) {
    string jstr, line;
    ifstream in(path);
    while (!in.eof()) {
        getline(in, line);
        jstr += line;
    }
    return _parse(jstr);
}
