/**
 * World manager
 *
 * Kai W(kkw)
 */
#ifndef WORLD_MANAGER_H
#define WORLD_MANAGER_H

#include <string>
using namespace std;

class jobject;

namespace world_manager {

  void init();
  /* I - type */
  void spawn_mob(string);
  /**
   * Initialized scene graph
   *
   * I - width, height
   */
  void init_scene_graph(int, int);
  /* Initializes world map (tiles) */
  void init_tiles(jobject &);
  /* Spawns initial mobs */
  void init_mobs(jobject &);

};
#endif
