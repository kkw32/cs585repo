/**
 * scene manager
 * 
 * A singleton manages game graph states through out the game
 *
 * Kai W(kkw)
 */
#ifndef SMANAGER_H
#define SMANAGER_H

template <typename T> class garray;
class scene_node;
class scene_graph;

namespace smanager {

  void set_graph(scene_graph *);
  scene_graph *get_graph();

  /**
   * Renders scene graph
   */
  void render();
  void clear();

};
#endif
