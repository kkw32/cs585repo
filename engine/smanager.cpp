#include <engine/scene_graph.h>
#include <engine/actor.h>
#include <engine/common.h>
#include <engine/smanager.h>
#include <utils/ghashmap.h>
#include <utils/garray.h>

scene_graph *graph;

void smanager::set_graph(scene_graph *g) {
  graph=g;
}

scene_graph *smanager::get_graph() {
  return graph;
}

void smanager::render() {
  graph->do_render();
}

void smanager::clear() {
  delete graph;
}
