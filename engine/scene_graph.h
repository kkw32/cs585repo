/**
 * Scene graph
 *
 * Kai W(kkw)
 */
#ifndef SCENE_GRAPH_H
#define SCENE_GRAPH_H

#include <vector>

class scene_node;

class scene_graph {
  public:
    /**
     * add a node into scene graph
     *
     * I - coords, pointer to the adding node
     */
    virtual void add_node(int, int, scene_node *) = 0;
    /**
     * remove a node from scene graph, however the node is not deleted, it's
     * only hidden from the graph
     *
     * I - coords, id(address) of the removing node's owner
     */
    virtual scene_node *remove_node(int, int, uintptr_t) = 0;
    /**
     * Gets node at (coord-x, coord-y)
     *
     * I - coords
     */
    virtual scene_node *get_node(int, int) = 0;
    /**
     * Gets node at (cursor_coord-x, cursor_coord-y)
     *
     * I - cursor_coords
     */
    virtual scene_node *get_node_fromcur(int, int) = 0;
    /**
     * Maps cursor coords to real coords from camera(0, 0) to world(0, 0)
     *
     * I - cursor_coords
     * O - coords
     */
    virtual void map_coords(int &, int &) = 0;
    /**
     * Tests if the coords provided is bound in graph
     *
     * I - coords
     */
    virtual bool bound(int, int) = 0;
    /**
     * Tests if the coords has a node
     *
     * I - coord
     */
    virtual bool has_node(int, int) = 0;
    /**
     * Called each frame to render all nodes
     */
    virtual void do_render() = 0;
    virtual int get_height() = 0;
    virtual int get_width() = 0;
    /**
     * Moves a node to the coords
     *
     * I - coords
     */
    virtual bool move_pos(int, int) = 0;
    /**
     * Gets colliders of a node, according to the node's bindbox
     *
     * I - node
     * O - all colliders of the node
     */
    virtual garray<scene_node *> get_colliders(scene_node *) = 0;
    virtual ~scene_graph(){};
};
#endif
