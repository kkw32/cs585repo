/**
 * eventbus
 * 
 * A singleton manages in-game events, who basically maintains relations between
 * callbacks and dispatchers, and also dispatches events.
 *
 * Kai W(kkw)
 */
#ifndef EVENTBUS_H
#define EVENTBUS_H

#include <string>
#include <cstdio>
#include <engine/handler.h>
#include <engine/common.h>
#include <engine/event.h>
#include <utils/gqueue.h>
#include <utils/ghashmap.h>
using namespace std;

class eventbus {

private:

  /**
   * Event queue
   */
  static gqueue<event *>             events;
  /**
   * key - event type
   * val - ghashmap of event handlers
   *       key - memory address of handler (should use unique-id)
   *       val - handler
   */
  static ghashmap<ghashmap<void *>> ltable;

public:

  /**
   * Immediatly dispatches an event to all registered handlers.
   *
   * I e - event
   */
  static void dispatch(event *e);

  /**
   * Registers an handler to a specific event type
   *
   * I etype - event type
   *   cb    - handler
   */
  template <typename T> static void add_listener(string etype, handler<T> *cb) {
    char mkey[32];
    sprintf(mkey, "%lu", reinterpret_cast<uintptr_t>(cb));
    ltable[etype][mkey]=static_cast<void *>(cb);
  }

  /**
   * Unregisters an handler from a specific event type
   *
   * I etype - event type
   *   cb    - handler
   */
  template <typename T> static void rm_listener(string etype, handler<T> *cb) {
    char mkey[32];
    sprintf(mkey, "%lu", reinterpret_cast<uintptr_t>(cb));
    ltable[etype].erase(mkey);
  }

  /**
   * Posts an event onto event queue.
   *
   * I e - event
   */
  static void post(event *e);

  /**
   * Gets all events from event queue and hanles them
   */
  static void run();

  static void shutdown();

};
#endif
