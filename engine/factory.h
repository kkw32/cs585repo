/**
 * Factory
 *
 * Kai W(kkw)
 */
#ifndef FACTORY_H
#define FACTORY_H

#include <utils/ghashmap.h>
#include <engine/maker.h>
#include <string>
using namespace std;

class factory {

private:

  /**
   * key - entity type
   * val - pointer to maker
   */
  static ghashmap<void *> makers;

public:

  /* I - entity type, maker */
  template <typename T> static void set_maker(string type, maker<T> *m) {
    makers[type]=static_cast<void *>(m);
  }

  template <typename T> static T make(string type) {
    return static_cast<maker<T> *>(makers[type])->make(type);
  }

};
#endif
