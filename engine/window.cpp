#include <engine/jparser.h>
#include <engine/window.h>
#include <fstream>
#include <iostream>

int    window::cam_h, window::cam_w;
int    window::inf_h, window::inf_w;
int    window::cur_x, window::cur_y;
WINDOW *window::cam_win;
WINDOW *window::inf_win;

void window::init() {

  int ch=32;
  int cw=142;
  cam_h=ch-2;
  cam_w=cw-2;
  inf_h=6;
  inf_w=82;

  /**
   * Calculation the center coords of screen
   */
  int max_x, max_y;
  getmaxyx(stdscr, max_x, max_y);
  int cam_x=(max_x-ch)/2;
  int cam_y=(max_y-cw)/2;

  cam_win=newwin(ch, cw, cam_x, cam_y);
  box(cam_win, 0, 0);
  wrefresh(cam_win);

  int inf_x=cam_x+ch+2;
  int inf_y=cam_y;
  inf_win=newwin(inf_h, inf_w, inf_x, inf_y);

  /**
   * (0, 0) is the box corner, so we +1 to (x, y)
   */
  cur_x=1;
  cur_y=1;

}

int window::bound(int x, int y) {
  return x>=1 && x<=cam_h && y>=1 && y<=cam_w;
}

bool window::mov_cur(int offx, int offy) {

  int nx=cur_x+offx;
  int ny=cur_y+offy;

  if (!bound(nx, ny)) {
    return 0;
  }

  cur_x=nx;
  cur_y=ny;

  return 1;

}

void window::show_cur() {
  wmove(cam_win, cur_x, cur_y);
  curs_set(1);
  wrefresh(cam_win);
}

void window::hide_cur() {
  curs_set(0);
  wrefresh(cam_win);
}
