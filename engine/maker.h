/**
 * Entity maker
 *
 * Kai W(kkw)
 */
#ifndef MAKER_H
#define MAKER_H

#include <engine/jobject.h>
#include <engine/jparser.h>
#include <string>
#include <unordered_map>
#include <vector>
using namespace std;

template <typename T> class maker {

  protected:
    /**
     * Configuration
     *
     * key - type
     * val - property configutation
     *       key - property name
     *       val - property value
     */
    unordered_map<string, shared_ptr<jobject>> config;
    /**
     * key - color name
     * val - color id
     */
    unordered_map<string, int> color_config;
    /**
     * Maker defination
     */
    typedef T (maker<T>::*make_func)(string);
    unordered_map<string, make_func> make_funcs;

    void parse(const string &&json, make_func f) {
        jparser jp;
        auto jo = jp.parse(json);

        for (int i = 0; i < jo->size(); i++) {
            string name = (*(*jo)[i])["name"]->jstring;
            config[name] = (*(*jo)[i])["data"];
            make_funcs[name] = f;
        }
    }

  public:
    unordered_map<string, shared_ptr<jobject>> &getConfig() { return config; }
    /* I - type */
    T make(string type) {
        make_func f = make_funcs[type];
        return (this->*f)(type);
    }
};
#endif
