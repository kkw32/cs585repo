/**
 * Event
 * 
 * Kai W(kkw)
 */
#ifndef EVENT_H
#define EVENT_H

#include <string>
using namespace std;

class event {

protected:

  string type;

public:

  event(): type("") {}
  event(string _t): type(_t) {}

  string get_type() {
    return type;
  }

};
#endif
