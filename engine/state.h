/**
 * State header
 *
 * Kai W(kkw)
 */
#ifndef STATE_H
#define STATE_H

class actor;

class state { // Default state: null

  protected:
    int id;

  public:
    state() {}

    int get_id() { return id; }
    /**
     * Transits to next state according to transition event
     *
     * I - transition event
     */
    virtual shared_ptr<state> transit(int) { return make_shared<state>(); }
    /**
     * State entry sequence
     */
    virtual void enter() {}
    /**
     * Called each tick to update state
     *
     * I - current tick time
     */
    virtual void update(const int) {}
    /**
     * State left sequence
     */
    virtual void leave() {}
    /**
     * Dumps all info in state
     */
    virtual void dump() {}
};
#endif
