/**
 * logger
 *
 * A singleton logger lives through the whole game
 *
 * Kai W(kkw)
 */
#ifndef LOGGER_H
#define LOGGER_H

#include <string>
using namespace std;

namespace logger {

/**
 * log files fall into 3 vertical categories, and 3 horizontal ones:
 *
 * V: debug, unittest and release
 * H: warn, error and log
 */
extern const int DBG_WARN;
extern const int DBG_ERRO;
extern const int DBG_LOGG;

extern const int UNI_WARN;
extern const int UNI_ERRO;
extern const int UNI_LOGG;

extern const int PLY_WARN;
extern const int PLY_ERRO;
extern const int PLY_LOGG;

void init();
/**
 * Adds and removes a logging source
 *
 * I - file path
 *
 * Normally, a local file is created as below:
 * int fd=open(path, O_CREAT | O_WRONLY, 00666);
 * Or a network file descriptor (socket, pipe etc).
 */
void add_source(const string&);
void remove_source(const string&);

void log(const int, const char *, ...);

}; // namespace logger
#endif
