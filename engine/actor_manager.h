/**
 * Actor factory
 *
 * Kai W(kkw)
 */
#ifndef ACTOR_MANAGER_H
#define ACTOR_MANAGER_H

#include <string>
using namespace std;

template <typename T> class garray;
class actor;

namespace actor_manager {

  void add_actor(actor *);
  void remove_actor(actor *);
  /* I - actor, new coord-x, new coord-y */
  void update_actor(actor *, int, int);
  /**
   * Gets actor by coords
   *
   * I - coords
   */
  actor *get_actor(int, int);

  /* Tickable actor */
  void add_tick(actor *);
  void remove_tick(actor *);
  void clear_deads();

  garray<actor *> all_ticks();
  garray<actor *> all_actors();

  void clear();
};
#endif
