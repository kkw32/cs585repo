/**
 * Scene node
 *
 * Kai W(kkw)
 */
#ifndef SCENE_NODE_H
#define SCENE_NODE_H

class actor;

class scene_node {

private:

  actor *who;

public:

  scene_node(actor *owner) {
    who=owner;
  }

  actor *get_owner() {
    return who;
  }

};
#endif
