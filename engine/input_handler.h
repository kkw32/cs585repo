/**
 * input manager
 * 
 * Kai W(kkw)
 */
#ifndef INPUT_HANDLER_H
#define INPUT_HANDLER_H

namespace input_handler {

  void update();

  bool nokey_pressed();
  /* I - key */
  bool is_pressed(int);

};
#endif
