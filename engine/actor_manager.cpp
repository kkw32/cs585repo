#include <utils/ghashmap.h>
#include <utils/garray.h>
#include <engine/actor.h>
#include <engine/common.h>
#include <engine/state.h>
#include <engine/smanager.h>
#include <engine/scene_graph.h>
#include <engine/scene_node.h>
#include <engine/actor_manager.h>

ghashmap<actor *>  nodes;
ghashmap<actor *>  ticks;

void actor_manager::add_actor(actor *a) {

  char mkey[32];
  sprintf(mkey, "%lu", reinterpret_cast<uintptr_t>(a));
  nodes[mkey]=a;

  int x=a->get_x();
  int y=a->get_y();
  int w=a->get_width();
  int h=a->get_height();

  for (int i=x, j=0; j<h; j++)
    for (int k=y, l=0; l<w; l++) {
      smanager::get_graph()->add_node(i+j, k+l, new scene_node(a));
    }

}

void actor_manager::remove_actor(actor *a) {

  char mkey[32];
  sprintf(mkey, "%lu", reinterpret_cast<uintptr_t>(a));

  if (nodes.count(mkey)) {

    int x=a->get_x();
    int y=a->get_y();
    int w=a->get_width();
    int h=a->get_height();

    for (int i=x, j=0; j<h; j++)
      for (int k=y, l=0; l<w; l++) {
        smanager::get_graph()->remove_node(i+j, k+l, reinterpret_cast<uintptr_t>(a));
      }

    delete a;
    nodes.erase(mkey);

    if (ticks.count(mkey)) {
      ticks.erase(mkey);
    }

  }

}

void actor_manager::update_actor(actor *a, int nx, int ny) {

    int x=a->get_x();
    int y=a->get_y();
    int w=a->get_width();
    int h=a->get_height();

    garray<scene_node *> temp;
    for (int i=x, j=0; j<h; j++)
      for (int k=y, l=0; l<w; l++) {
        scene_node *r=smanager::get_graph()->remove_node(i+j, k+l, reinterpret_cast<uintptr_t>(a));
        temp.push_back(r);
      }

    int index=0;
    for (int i=nx, j=0; j<h; j++)
      for (int k=ny, l=0; l<w; l++) {
        smanager::get_graph()->add_node(i+j, k+l, temp[index++]);
      }

    a->set_x(nx);
    a->set_y(ny);

}

actor *actor_manager::get_actor(int x, int y) {
  return smanager::get_graph()->get_node(x, y)->get_owner();
}

void actor_manager::add_tick(actor *a) {

  add_actor(a);

  char mkey[32];
  sprintf(mkey, "%lu", reinterpret_cast<uintptr_t>(a));
  ticks[mkey]=a;

}

void actor_manager::clear_deads() {
  garray<string> keys=ticks.keys();
  for (int i=0; i<keys.size(); i++) {
    actor *n=ticks[keys[i]];
    if (n->is_dead()) {
      remove_actor(n);
    }
  }
}

garray<actor *> actor_manager::all_ticks() {

  garray<actor *> ret;

  garray<string> keys=ticks.keys();
  for (int i=0; i<keys.size(); i++) {
    ret.push_back(ticks[keys[i]]);
  }

  return ret;

}

garray<actor *> actor_manager::all_actors() {

  garray<actor *> ret;
  garray<string> keys=nodes.keys();
  for (int i=0; i<keys.size(); i++) {
    ret.push_back(nodes[keys[i]]);
  }

  return ret;

}

void actor_manager::clear() {
  garray<string> keys=nodes.keys();
  for (int i=0; i<keys.size(); i++) {
    delete nodes[keys[i]];
  }
}
