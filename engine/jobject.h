/**
 * JSON object
 *
 * Kai W(kkw)
 */
#ifndef JOBJECT_H
#define JOBJECT_H

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>
using namespace std;

const unsigned char JSTRING = 0x01;
const unsigned char JINT = 0x02;
const unsigned char JDOUBLE = 0x03;
const unsigned char JBOOL = 0x04;
const unsigned char JLIST = 0x05;
const unsigned char JDICT = 0x06;
const unsigned char JNULL = 0x07;
const unsigned char JEMPTY = 0x08;

class jobject {
  private:
    unsigned char type;
    vector<shared_ptr<jobject>> list;
    unordered_map<string, shared_ptr<jobject>> dict;

  public:
    union {
        long long jint; // 64b signed
        double jdouble;
        bool jbool;
        int jnull;
    };
    string jstring;

    jobject(string);
    jobject(double);
    jobject(bool);
    jobject();
    jobject(long long);
    jobject(unsigned char);

    shared_ptr<jobject> &operator[](const int);    // list subscript
    shared_ptr<jobject> &operator[](const string&); // dict subscript

    unsigned char get_type() { return type; }

    /**
     * Gets size of the JSON object, according to object type
     *
     * O - string length || list length || dict size
     */
    int size();

    /**
     * Adds a jobject to list jobject
     *
     * I - jojbect
     */
    void add(const shared_ptr<jobject>);

    /**
     * Gets the key of dict jobject
     *
     * O - array of key set
     */
    vector<string> keys();
};

#endif
