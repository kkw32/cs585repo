/**
 * simple JSON parser parsing a JSON string into a c++ JSON object
 *
 * would change stlmap to my own ghashmap in the future
 *
 * Kai W(kkw)
 */
#ifndef JPARSER_H
#define JPARSER_H

#include <string>
using namespace std;

class jobject;

class jparser {

  private:
    int json_strlen;
    string json_str;

    /* Tests the given char */
    bool expsign(char x); // - | +
    bool exp(char x);     // e | E

    /**
     * Skipps all spaces
     *
     * O - new cursor position
     */
    void trim_space(int &);
    /**
     * Skipps all digits
     *
     * O - new cursor position
     */
    void trim_digit(int &);
    /**
     * Base case, tests whether the given string is true, false or null
     *
     * O - new cursor position
     */
    void basic_string(int &, const string &);
    /**
     * Recursively parses a JSON string
     *
     * base case is when current cursor is at the start of JSTRING, JBOOL, JNULL
     * and JINT. Or in general case, JLIST and JDICT are recursively parsed.
     *
     * i - current cursor position
     */
    shared_ptr<jobject> _parse(int &);
    shared_ptr<jobject> _parse(const string &);

  public:
    /**
     * Parses the given json string. It calls _parse(string)
     *
     * I - json string
     */
    shared_ptr<jobject> parse(const string &);
};
#endif
