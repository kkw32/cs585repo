/**
 * Event handler
 *
 * Kai W(kkw)
 */
#ifndef HANDLER_H
#define HANDLER_H

class event;
class eventbus;

template <typename T> class handler {

  /* Allows eventbus to access _handle_event */
  friend class eventbus;

protected:

  void _handle_event(event *e) {
    handle_event(static_cast<T>(e));
  }

public:

  virtual void handle_event(T e)=0;

};
#endif
