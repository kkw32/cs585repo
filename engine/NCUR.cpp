#include <ncurses.h>
#include <engine/NCUR.h>

void NCUR::init() {

  /* initialize */
  initscr();
  noecho();
  curs_set(0);

  /* input */
  cbreak();
  nodelay(stdscr, 1);
  keypad(stdscr, 1);

  /* color */
  use_default_colors();
  start_color();

}

void NCUR::end() {
  endwin();
}
