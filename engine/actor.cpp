#include <engine/actor.h>
#include <engine/state.h>
#include <engine/window.h>
#include <ncurses.h>

actor::actor(string _n):
  name(_n),
  width(1),
  height(1),
  x(0),
  y(0),
  tile_type(-1),
  tar_x(-1),
  tar_y(-1),
  org_x(-1),
  org_y(-1),
  team(-1),
  health(1),
  health_cur(1),
  dmg_min(0),
  dmg_max(1),
  def_min(0),
  def_max(1),
  dmg_bonus(0),
  def_bonus(0),
  att_interval(0),
  dead(0),
  chosen(0),
  in_combat(0),
  rep_char(' '),
  color(0)
  {
  //
}

void actor::take_dmg(int d) {
  health_cur-=d;
  if (health_cur<=0) {
    dead=1;
  }
}

void actor::update(int new_tick) {
  states.top()->update(new_tick);
}

void actor::draw(int dx, int dy) {
  char rep_temp=rep_char;
  wattron(window::cam_win, COLOR_PAIR(color));
  if (chosen) {
    rep_temp='_';
  }
  mvwaddch(window::cam_win, dx+1, dy+1, rep_temp);
  wattroff(window::cam_win, COLOR_PAIR(color));
}

state *actor::cur_state() {
  return states.top();
}

void actor::push_state(state *s) {
  states.push(s);
}

void actor::pop_state() {
  delete states.top();
  states.pop();
}

bool actor::collide(actor *o) {
  return 0;
}

void actor::dump() {
  mvwprintw(window::inf_win, 1, 10, "%s", name.c_str());
  states.top()->dump();
}

bool actor::is_player() {
  return 0;
}

bool actor::is_building() {
  return 0;
}

actor::~actor() {
  while (!states.empty()) {
    delete states.top();
    states.pop();
  }
}
