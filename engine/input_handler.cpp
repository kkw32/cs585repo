#include <engine/input_handler.h>
#include <ncurses.h>

int pressed=-1;

void input_handler::update() {
  pressed=getch();
}

bool input_handler::nokey_pressed() {
  return pressed==-1;
}

bool input_handler::is_pressed(int key) {
  return pressed==key;
}
