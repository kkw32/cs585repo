#include <engine/actor.h>
#include <engine/scene_node.h>
#include <engine/scene_graph.h>
#include <engine/window.h>
#include <engine/common.h>
#include <engine/fixed_grid.h>
#include <ncurses.h>

fixed_grid::fixed_grid(int h, int w) {

  height=h, width=w;
  map=new garray<scene_node *> **[h];

  for (int i=0; i<h; i++) {
    map[i]=new garray<scene_node *> *[w];
    for (int j=0; j<w; j++) {
      map[i][j]=new garray<scene_node *>();
    }
  }

  pos_x=0;
  pos_y=0;

}

void fixed_grid::add_node(int x, int y, scene_node *n) {
  if (!bound(x, y)) {
    return;
  }
  map[x][y]->push_back(n);
}

scene_node *fixed_grid::remove_node(int x, int y, uintptr_t n) { 

  scene_node *ret=0;

  if (!bound(x, y)) {
    return ret;
  }

  garray<scene_node *> *nodes=map[x][y];
  for (int i=nodes->size()-1; i; i--)
    if (reinterpret_cast<uintptr_t>(
          nodes->garray<scene_node *>::operator[](i)->get_owner()
        )==n) {
    ret=nodes->garray<scene_node *>::operator[](i);
    nodes->erase(i);
    break;
  }

  return ret;

}

scene_node *fixed_grid::get_node(int x, int y) {
  if (!bound(x, y)) {
    return 0;
  }
  return map[x][y]->last();
}

scene_node *fixed_grid::get_node_fromcur(int x, int y) {
  return map[x+pos_x][y+pos_y]->last();
}

void fixed_grid::map_coords(int &x, int &y) {
  x+=pos_x;
  y+=pos_y;
}

bool fixed_grid::bound(int x, int y) {
  return x>=0 && x<height && y>=0 && y<width;
}

bool fixed_grid::has_node(int x, int y) {
  if (!bound(x, y)) {
    return 0;
  }
  return map[x][y]->size()!=0;
}

void fixed_grid::do_render() {
  for (int i=pos_x; i<pos_x+window::cam_h; i++)
    for (int j=pos_y; j<pos_y+window::cam_w; j++) {
      map[i][j]->last()->get_owner()->draw(i-pos_x, j-pos_y);
    }
  box(window::cam_win, 0, 0);
  wrefresh(window::cam_win);
}

bool fixed_grid::move_pos(int offx, int offy) {

  int nx=pos_x+offx;
  int ny=pos_y+offy;

  if (!bound(nx, ny) || !bound(nx+window::cam_h, ny+window::cam_w)) {
    return 0;
  }

  pos_x=nx;
  pos_y=ny;

  wmove(window::inf_win, 1, 1);
  wclrtoeol(window::inf_win);
  mvwprintw(window::inf_win, 1, 1, "%-4d %-4d", pos_x, pos_y);

  return 1;

}

garray<scene_node *> fixed_grid::get_colliders(scene_node *n) {
  return garray<scene_node *>();
}

fixed_grid::~fixed_grid() {
  for (int i=0; i<height; i++) {
    for (int j=0; j<width; j++) {
      while (map[i][j]->size()!=0) {
        scene_node *n=map[i][j]->last();
        map[i][j]->erase(map[i][j]->size()-1);
        delete n;
      }
      delete map[i][j];
    }
    delete[] map[i];
  }
  delete[] map;
}
