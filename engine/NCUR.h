/**
 * ncurses wrapper
 *
 * Kai W(kkw)
 */
#ifndef NCUR_H
#define NCUR_H

namespace NCUR {

  void init();
  void end();

};
#endif
