#include <engine/jobject.h>

jobject::jobject(string s) { type = JSTRING, jstring = s; }
jobject::jobject(double d) { type = JDOUBLE, jdouble = d; }
jobject::jobject(bool b) { type = JBOOL, jbool = b; }
jobject::jobject() { type = JNULL, jnull = 0; }
jobject::jobject(long long i) { type = JINT, jint = i; }
jobject::jobject(unsigned char t) { type = t; }

shared_ptr<jobject> &jobject::operator[](const int i) {
    if (type != JLIST) {
        throw runtime_error("jobject::operator[](const int): index subscript can "
                            "only be used on array object");
    }

    if (i < 0 || i >= list.size()) {
        throw length_error("jobject::operator[](const int): invalid index");
    }

    return list[i];
}

shared_ptr<jobject> &jobject::operator[](const string &key) {
    if (type != JDICT) {
        throw runtime_error("jobject::operator[](const string&): string "
                            "subscript can only be used on map object");
    }

    return dict[key];
}

void jobject::add(const shared_ptr<jobject> j) { list.push_back(j); }

int jobject::size() {
    switch (type) {
    case JSTRING:
        return jstring.length();
    case JLIST:
        return list.size();
    case JDICT:
        return dict.size();
    default:
        break;
    }
    return -1;
}

vector<string> jobject::keys() {
    if (type == JDICT) {
        vector<string> ret;
        for (const auto &[k, _] : dict) {
            ret.push_back(k);
        }
        return ret;
    } else {
        return {};
    }
}
