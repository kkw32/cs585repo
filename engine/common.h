/**
 * Common typedef, macro, constants
 *
 * Kai W(kkw)
 */
#ifndef COMMON_H
#define COMMON_H

#include <string>
using namespace std;

namespace ENGINE {

  const int STATE_NULL =0x0000;

  const int MOVE_H     =0x0001;
  const int MOVE_V     =0x0002;

  const int TILE_OBS   =0x0000;
  const int TILE_CAN   =0x0001;
  const int TILE_MOB   =0x0002;
  const int TILE_PLY   =0x0003;

  const int TEAM_PLAYER=0x0001;

}
#endif
