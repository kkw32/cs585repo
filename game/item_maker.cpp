#include <engine/factory.h>
#include <engine/jparser.h>
#include <game/item_maker.h>

shared_ptr<item> item_maker::_make_item(const string &type) {
    auto i = make_shared<item>(type);

    auto ca = config[type];

    vector<int> price_range;
    auto pr = (*ca)["price_range"];
    for (int i = 0; i < pr->size(); i++) {
        price_range.push_back((*pr)[i]->jint);
    }
    i->set_price_range(price_range);

    vector<int> bonus_range;
    auto br = (*ca)["bonus_range"];
    for (int i = 0; i < br->size(); i++) {
        bonus_range.push_back((*br)[i]->jint);
    }
    i->set_bonus_range(bonus_range);

    return i;
}

item_maker::item_maker() {
    parse("game/item.json", static_cast<make_func>(&item_maker::_make_item));
}
