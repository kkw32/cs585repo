/**
 * Building state: normal state
 *
 * Kai W(kkw)
 */
#ifndef BUILDING_STATE_H
#define BUILDING_STATE_H

#include <engine/state.h>

class hall;

class building_state : public state {

private:

  hall *inst;

public:

  building_state(actor *);

  state *transit(int);
  void enter();
  void update(int);
  void leave();
  void dump();

};
#endif
