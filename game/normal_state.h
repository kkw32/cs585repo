/**
 * Player state: normal state
 *
 * Kai W(kkw)
 */
#ifndef NORMAL_STATE_H
#define NORMAL_STATE_H

#include <game/player_state.h>

class normal_state : public player_state {

private:

  int thirst;
  int sleep;

  int hunting;

public:

  normal_state(actor *);

  state *transit(int);
  void enter();
  void update(int);
  void leave();
  void dump();

};
#endif
