#include <engine/smanager.h>
#include <engine/actor_manager.h>
#include <engine/scene_graph.h>
#include <engine/scene_node.h>
#include <engine/common.h>
#include <engine/eventbus.h>
#include <game/gcommon.h>
#include <game/state_event.h>
#include <game/combat_state.h>
#include <game/player_state.h>
#include <ncurses.h>

bool player_state::attack(int nx, int ny) {

  actor *n=smanager::get_graph()->get_node(nx, ny)->get_owner();

  if (n->get_type()==ENGINE::TILE_MOB && !n->is_incombat()) {
    inst->set_combat(1);
    n->set_combat(1);
    eventbus::post(
      new state_event(GAME::EVENT_SPUSH, new combat_state(inst, n), inst)
    );
    eventbus::post(
      new state_event(GAME::EVENT_SPUSH, new combat_state(n, inst), n)
    );
    return 1;
  }

  return 0;

}

void player_state::route_manual() {

  int x=inst->get_tarx();
  int y=inst->get_tary();
  int nx=inst->get_x();
  int ny=inst->get_y();

  /* Target reached */
  if (nx==x && ny==y) {
    inst->set_tarx(-1);
    inst->set_tary(-1);
  }

  /* Routing */
  if (x!=-1) {
    if (nx<x) {
      nx++;
    } else if (nx>x) {
      nx--;
    }
    if (ny<y) {
      ny++;
    } else if (ny>y) {
      ny--;
    }
    /* Gets into combat if target is a mob */
    if (nx==x && ny==y) {
      if (attack(nx, ny)) {
        return;
      }
    }
    actor_manager::update_actor(inst, nx, ny);
  }

}

void player_state::route_auto() {

  /* If a target is assigned, do route_manual */
  int tx=inst->get_tarx();
  if (tx!=-1) {
    route_manual();
    return;
  }

  int x=inst->get_x();
  int y=inst->get_y();
  int nx, ny, d;

  do {
    d=rand()%4;                       // random direction
    switch (d) {
      case 0: nx=x-1; ny=y;   break;  // up
      case 1: nx=x;   ny=y-1; break;  // left
      case 2: nx=x+1; ny=y;   break;  // down
      case 3: nx=x;   ny=y+1; break;  // right
    }
  } while (!smanager::get_graph()->bound(nx, ny) ||
           smanager::get_graph()->get_node(nx, ny)->get_owner()->get_type()==
           ENGINE::TILE_OBS);

  if (attack(nx, ny)) {
    return;
  }

  actor_manager::update_actor(inst, nx, ny);

}
