#include <engine/jparser.h>
#include <engine/jobject.h>
#include <engine/state.h>
#include <engine/common.h>
#include <game/normal_state.h>
#include <game/building_state.h>
#include <game/player.h>
#include <game/mob.h>
#include <game/hall.h>
#include <game/actor_maker.h>
#include <ncurses.h>

actor *actor_maker::_make_player(string type) {

  jobject       &ca=config[type];
  ghashmap<int> &cc=color_config;

  player *p=new player(type);

  p->set_drawable(
    ca["rep_char"].jstring[0],
    cc[ca["color"].jstring]
  );
  p->set_type(ENGINE::TILE_PLY);
  p->set_width(ca["width"].jint);
  p->set_height(ca["height"].jint);

  p->set_team(ENGINE::TEAM_PLAYER);
  p->set_health(ca["basic_health"].jint);
  p->set_health_cur(ca["basic_health"].jint);
  p->set_dmg(ca["dmg_min"].jint, ca["dmg_max"].jint);
  p->set_def(ca["def_min"].jint, ca["def_max"].jint);
  p->set_att_interval(ca["att_interval"].jint);

  p->set_level_max(ca["level_max"].jint);
  garray<int> exp_range;
  for (int i=0; i<ca["exp_range"].size(); i++) {
    exp_range.push_back(ca["exp_range"][i].jint);
  }
  p->set_exp_range(exp_range);

  p->set_thirst_rate(ca["thirst_rate"].jint);
  p->set_thirst_max(ca["thirst_max"].jint);
  p->set_drink_time(ca["drink_time"].jint);
  p->set_sleep_rate(ca["sleep_rate"].jint);
  p->set_sleep_max(ca["sleep_max"].jint);
  p->set_sleep_time(ca["sleep_time"].jint);

  p->push_state(new normal_state(p));

  return p;

}

actor *actor_maker::_make_mob(string type) {

  jobject       &ca=config[type];
  ghashmap<int> &cc=color_config;

  mob *m=new mob(type);

  m->set_drawable(
    ca["rep_char"].jstring[0],
    cc[ca["color"].jstring]
  );
  m->set_type(ENGINE::TILE_MOB);
  m->set_width(ca["width"].jint);
  m->set_height(ca["height"].jint);

  m->set_health(ca["basic_health"].jint);
  m->set_health_cur(ca["basic_health"].jint);
  m->set_dmg(ca["dmg_min"].jint, ca["dmg_max"].jint);
  m->set_def(ca["def_min"].jint, ca["def_max"].jint);
  m->set_att_interval(ca["att_interval"].jint);

  m->set_loot_min(ca["loot_min"].jint);
  m->set_loot_max(ca["loot_max"].jint);
  m->set_exp(ca["exp"].jint);

  m->push_state(new state());

  return m;

}

actor *actor_maker::_make_tile(string type) {

  jobject       &ca=config[type];
  ghashmap<int> &cc=color_config;

  actor *t=new actor(type);

  t->set_drawable(
    ca["rep_char"].jstring[0],
    cc[ca["color"].jstring]
  );
  t->set_type(ca["tile_type"].jint);

  t->push_state(new state());

  return t;

}

actor *actor_maker::_make_building(string type) {

  jobject       &ca=config[type];
  ghashmap<int> &cc=color_config;

  hall *h=new hall(type);

  h->set_drawable(
    ca["rep_char"].jstring[0],
    cc[ca["color"].jstring]
  );
  h->set_type(ENGINE::TILE_PLY);
  h->set_width(ca["width"].jint);
  h->set_height(ca["height"].jint);

  h->set_team(ENGINE::TEAM_PLAYER);
  h->set_dwarf_cost(ca["dwarf_cost"].jint);
  h->set_level_ale_max(ca["level_ale_max"].jint);
  h->set_level_potion_max(ca["level_potion_max"].jint);
  h->set_level_weapon_max(ca["level_weapon_max"].jint);
  h->set_level_armor_max(ca["level_armor_max"].jint);
  h->set_tax_rate(ca["tax_rate"].jint);
  h->set_apothecary_cost(ca["apothecary_cost"].jint);
  h->set_blacksmith_cost(ca["blacksmith_cost"].jint);

  garray<int> up_range;
  for (int i=0; i<ca["upgrade_range"].size(); i++) {
    up_range.push_back(ca["upgrade_range"][i].jint);
  }
  h->set_upgrade_range(up_range);

  h->push_state(new building_state(h));

  return h;

}

actor_maker::actor_maker() {

  parse("game/player.json", static_cast<make_func>(&actor_maker::_make_player));
  parse("game/mob.json", static_cast<make_func>(&actor_maker::_make_mob));
  parse("game/tile.json", static_cast<make_func>(&actor_maker::_make_tile));
  parse("game/building.json", static_cast<make_func>(&actor_maker::_make_building));

  ghashmap<int> &cc=color_config;

  jparser jp;
  jobject colors=jp.parse("game/color.json");

  garray<string> keys=colors.keys();
  for (int i=0; i<keys.size(); i++) {
    jobject rgb=colors[keys[i]];
    init_color(i+1, rgb[0].jint, rgb[1].jint, rgb[2].jint);
    init_pair(i+1, i+1, -1);
    cc[keys[i]]=i+1;
  }

}
