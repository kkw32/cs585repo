/**
 * In-game actor event
 *
 * Kai W(kkw)
 */
#ifndef ACTOR_EVENT_H
#define ACTOR_EVENT_H

#include <engine/event.h>

class actor;

class actor_event : public event {

protected:

  actor *who;

public:

  /* I - event source, the actor */
  actor_event(string t, actor *a): event(t) {
    who=a;
  }

  actor *get_actor() { return who; }

};
#endif
