/**
 * In-game state transition event, STATE_POP and STATE_PUSH
 *
 * Kai W(kkw)
 */
#ifndef STATE_EVENT_H
#define STATE_EVENT_H

#include <engine/event.h>
#include <engine/state.h>

class actor;

class state_event : public event {

private:

  state *state_to;
  actor *who;

public:

  /* I - state transit to, event source actor */
  state_event(string t, state *to, actor *a): event(t) {
    state_to=to;
    who=a;
  }

  state *get_state() { return state_to; }
  actor *get_actor() { return who; }

};
#endif
