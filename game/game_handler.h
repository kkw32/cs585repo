/**
 * Game event handler
 *
 * Kai W(kkw)
 */
#ifndef GAME_HANDLER_H
#define GAME_HANDLER_H

#include <engine/handler.h>

class game;
class game_event;

class game_handler : public handler<game_event *> {

private:

  game * g;

public:

  game_handler() {}
  game_handler(game *_g): g(_g) {}

  void handle_event(game_event *e);

};
#endif
