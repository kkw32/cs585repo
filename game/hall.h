/**
 * Hall actor
 *
 * Kai W(kkw)
 */
#ifndef HALL_H
#define HALL_H

#include <utils/garray.h>
#include <engine/actor.h>

class game;

class hall : public actor {

private:

  int level_ale;
  int level_potion;
  int level_weapon;
  int level_armor;

  int level_ale_max;
  int level_potion_max;
  int level_weapon_max;
  int level_armor_max;

  int num_dwarf;
  int num_wizard;

  int dwarf_cost;
  int apothecary_cost;
  int blacksmith_cost;
  int tax_rate;
  // int wizard_cost;
  //
  bool apothecary;
  bool blacksmith;

  garray<int> upgrade_range;

  game *g;

public:

  hall(string);

  int get_level_ale()                    { return level_ale; }
  int get_level_potion()                 { return level_potion; }
  int get_level_weapon()                 { return level_weapon; }
  int get_level_armor()                  { return level_armor; }

  int get_num_dwarf()                    { return num_dwarf; }
  int get_num_wizard()                   { return num_wizard; }
  int get_tax_rate()                     { return tax_rate; }

  bool has_apothecary()                  { return apothecary; }
  bool has_blacksmith()                  { return blacksmith; }

  void set_level_ale_max(int m)          { level_ale_max=m; }
  void set_level_potion_max(int m)       { level_potion_max=m; }
  void set_level_weapon_max(int m)       { level_weapon_max=m; }
  void set_level_armor_max(int m)        { level_armor_max=m; }

  void set_upgrade_range(garray<int> ur) { upgrade_range=ur; }
  void set_dwarf_cost(int dc)            { dwarf_cost=dc; }
  void set_tax_rate(int tr)              { tax_rate=tr; }
  void set_game(game *_g)                { g=_g; }

  void set_apothecary_cost(int ac)       { apothecary_cost=ac; }
  void set_blacksmith_cost(int bc)       { blacksmith_cost=bc; }

  /* I - total money in the cache */
  void upgrade_level_ale();
  void upgrade_level_potion();
  void upgrade_level_weapon();
  void upgrade_level_armor();

  void recruit_dwarf();
  void release_dwarf()                   { num_dwarf--; }
  // TODO: recruit_wizard
  // void recruit_wizard();

  void hire_apothecary();
  void hire_blacksmith();

  bool is_building();

};
#endif
