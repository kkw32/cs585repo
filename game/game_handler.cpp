#include <engine/world_manager.h>
#include <game/game_event.h>
#include <game/game.h>
#include <game/gcommon.h>
#include <game/game_handler.h>

void game_handler::handle_event(game_event *e) {

  string type=e->get_type();

  if (type==GAME::EVENT_INCOME) {
    g->add_cache(e->get_int());
  } else if (type==GAME::EVENT_SPAWN) {
    world_manager::spawn_mob(e->get_str());
  }

}
