/**
 *  Game states.
 * 
 * Kai W(kkw)
 */
#ifndef GAME_STATE_H
#define GAME_STATE_H

#include <engine/state.h>

class game;

class game_state : public state {

protected:

  game *inst;

public:

  game_state(game *g): inst(g) {}

};
#endif
