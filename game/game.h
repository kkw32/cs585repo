/**
 * The game
 *
 * Kai W(kkw)
 */
#ifndef GAME_H
#define GAME_H

#include <engine/jobject.h>
#include <game/actor_maker.h>
#include <game/game_handler.h>
#include <game/item_maker.h>
#include <game/state_handler.h>

class state;

class game {
  private:
    shared_ptr<state> state_cur;

    /* Handlers */
    state_handler h_state;
    game_handler h_game;

    /* Makers */
    actor_maker m_actor;
    item_maker m_item;

    int move_cost;
    int cache;
    int frame_total;

    shared_ptr<jobject> spawn_cost;

  public:
    game() : frame_total(0) {}

    /* Accessors & Setters */
    int get_cache() { return cache; }
    int get_move_cost() { return move_cost; }
    int get_spawn_cost(string t) { return (*spawn_cost)[t]->jint; }
    void add_cache(int c) { cache += c; }

    bool can_move();
    bool can_spawn(const string &);

    void init();
    int logic();

    ~game();
};
#endif
