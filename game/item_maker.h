/**
 * Item maker (factory)
 *
 * Kai W(kkw)
 */
#ifndef ITEM_MAKER_H
#define ITEM_MAKER_H

#include <engine/item.h>
#include <engine/maker.h>

class item_maker : public maker<shared_ptr<item>> {

  private:
    shared_ptr<item> _make_item(const string&);

  public:
    item_maker();
};
#endif
