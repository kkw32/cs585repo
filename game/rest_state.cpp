#include <engine/window.h>
#include <engine/eventbus.h>
#include <game/game_event.h>
#include <game/state_event.h>
#include <game/gcommon.h>
#include <game/rest_state.h>

rest_state::rest_state(actor *p, int rate, int mtime): player_state(p) {
  id=GAME::ACT_STATE_SLP;
  rest_time=0;
  rest_rate=rate;
  rest_time_max=mtime;
}

state *rest_state::transit(int tevent) {
  return 0;
}

void rest_state::enter() {
  rest_time=0;
  route=get_route_manual();
  inst->go_home();
}

void rest_state::update(int new_tick) {

  (this->*route)();

  rest_time+=rest_rate;
  if (rest_time>=rest_time_max) {
    eventbus::post(new state_event(GAME::EVENT_SPOP, 0, inst));
  }

}

void rest_state::leave() {
  inst->go_back();
}

void rest_state::dump() {
  mvwprintw(window::inf_win, 1, 30, "resting: %4d/%4d", rest_time, rest_time_max);
}
