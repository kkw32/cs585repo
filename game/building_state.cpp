#include <engine/input_handler.h>
#include <engine/window.h>
#include <game/hall.h>
#include <game/building_state.h>

building_state::building_state(actor *b) {
  inst=static_cast<hall *>(b);
}

state *building_state::transit(int tevent) {
  return 0;
}

void building_state::enter() {
}

void building_state::update(int new_tick) {
  if (input_handler::is_pressed('u')) {
    inst->upgrade_level_ale();
  } else if (input_handler::is_pressed('i')) {
    inst->upgrade_level_potion();
  } else if (input_handler::is_pressed('o')) {
    inst->upgrade_level_weapon();
  } else if (input_handler::is_pressed('p')) {
    inst->upgrade_level_armor();
  } else if (input_handler::is_pressed('a')) {
    inst->recruit_dwarf();
  } else if (input_handler::is_pressed('d')) {
    //inst->recruit_wizard();
  } else if (input_handler::is_pressed('b')) {
    inst->hire_apothecary();
  } else if (input_handler::is_pressed('n')) {
    inst->hire_blacksmith();
  }
}

void building_state::leave() {
}

void building_state::dump() {
  mvwprintw(window::inf_win, 1, 30, "ALE   : lv.%3d", inst->get_level_ale());
  mvwprintw(window::inf_win, 1, 50, "dwarf     : %4d",    inst->get_num_dwarf());
  mvwprintw(window::inf_win, 2, 30, "POTION: lv.%3d", inst->get_level_potion());
  mvwprintw(window::inf_win, 2, 50, "apothecary: %4d", inst->has_apothecary());
  mvwprintw(window::inf_win, 3, 30, "WEAPON: lv.%3d", inst->get_level_weapon());
  mvwprintw(window::inf_win, 3, 50, "blacksmith: %4d", inst->has_blacksmith());
  mvwprintw(window::inf_win, 4, 30, "ARMOR : lv.%3d", inst->get_level_armor());
}
