/**
 * Actor maker (factory)
 *
 * Kai W(kkw)
 */
#ifndef ACTOR_MAKER_H
#define ACTOR_MAKER_H

#include <engine/maker.h>
#include <engine/actor.h>

class actor_maker : public maker<actor *> {

private:

  actor *_make_player(string);
  actor *_make_mob(string);
  actor *_make_tile(string);
  actor *_make_building(string);

public:

  actor_maker();

};
#endif
