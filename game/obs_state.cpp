#include <engine/smanager.h>
#include <engine/scene_graph.h>
#include <engine/scene_node.h>
#include <engine/window.h>
#include <engine/input_handler.h>
#include <engine/actor_manager.h>
#include <engine/factory.h>
#include <engine/common.h>
#include <game/game.h>
#include <game/hall.h>
#include <game/gcommon.h>
#include <game/move_state.h>
#include <game/normal_state.h>
#include <game/obs_state.h>

void obs_state::find_target() {

  if (!inst->can_move()) {
    return;
  }

  if (crossing->is_player()) {
    crossing->set_choice(1);
    temp=crossing;
    find=&obs_state::set_target;
    smanager::render();
  }

}

void obs_state::set_target() {

  if (temp->cur_state()->get_id()==GAME::ACT_STATE_PLY) {
    int x=window::cur_x-1;
    int y=window::cur_y-1;
    smanager::get_graph()->map_coords(x, y);
    temp->set_tarx(x);
    temp->set_tary(y);
    inst->add_cache(-inst->get_move_cost());
  }

  temp->set_choice(0);
  temp=0;
  find=&obs_state::find_target;
  smanager::render();

}

void obs_state::spawn_hall() {

  if (!inst->can_spawn("grand hall")) {
    return;
  }

  int x=window::cur_x-1;
  int y=window::cur_y-1;
  smanager::get_graph()->map_coords(x, y);

  hall *h=factory::make<hall *>("grand hall");
  for (int i=x, width=0; width<h->get_width(); width++)
    for (int j=y, height=0; height<h->get_height(); height++)
      if (smanager::get_graph()->get_node(i+width, j+height)->get_owner()->get_type()!=ENGINE::TILE_CAN) {
        delete h;
        return;
      }

  h->set_x(x);
  h->set_y(y);
  h->set_game(inst);
  actor_manager::add_tick(h);
  inst->add_cache(-inst->get_spawn_cost("grand hall"));
  smanager::render();

}

obs_state::obs_state(game *g): game_state(g) {
  id=GAME::STATE_OBS;
  find=&obs_state::find_target;
  temp=0;
  temp2=0;
  crossing=0;
}

state *obs_state::transit(int tevent) {
  if (tevent==' ') {
    return new move_state(inst);
  }
  return 0;
}

void obs_state::enter() {
  window::show_cur();
}

void obs_state::leave() {
  if (temp) {
    temp->set_choice(0);
    temp=0;
  }
  if (temp2) {
    temp2->set_choice(0);
    temp2=0;
  }
  crossing=0;
}

void obs_state::update(int new_tick) {

  /* Handle input: move */
  if (input_handler::is_pressed('h')) {
    window::mov_cur( 0, -1);
  } else if (input_handler::is_pressed('l')) {
    window::mov_cur( 0,  1);
  } else if (input_handler::is_pressed('k')) {
    window::mov_cur(-1,  0);
  } else if (input_handler::is_pressed('j')) {
    window::mov_cur( 1,  0);
  }

  int x=window::cur_x-1;
  int y=window::cur_y-1;
  smanager::get_graph()->map_coords(x, y);
  crossing=smanager::get_graph()->get_node(x, y)->get_owner();

  /* Handle input: logic */
  if (input_handler::is_pressed('f')) {  // call find_func
    (this->*find)();
  } else if (input_handler::is_pressed('s')) {  // spawn hall
    spawn_hall();
    crossing=smanager::get_graph()->get_node(x, y)->get_owner();
  }

  if (input_handler::nokey_pressed()) {
    return;
  }

  /* Every building the cursor is on will be chosen */
  if (crossing->is_building()) {
    if (!crossing->is_chosen()) {
      crossing->set_choice(1);
      if (temp2) {
        temp2->set_choice(0);
      }
      temp2=crossing;
    }
    crossing->update(new_tick);
    smanager::render();
  } else {
    if (temp2) {
      temp2->set_choice(0);
      temp2=0;
      smanager::render();
    }
  }

  window::hide_cur();
  /* Draws cache and coords */
  werase(window::inf_win);
  mvwprintw(window::inf_win, 0, 1, "%d", inst->get_cache());
  mvwprintw(window::inf_win, 1, 1, "%-4d %-4d", x, y);
  mvwprintw(window::inf_win, 3, 1, "ADDR: %X", crossing);
  /* Draws node */
  crossing->dump();

  wrefresh(window::inf_win);
  window::show_cur();

}
