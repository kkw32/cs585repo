/**
 * Combat state
 *
 * Kai W(kkw)
 */
#ifndef COMBAT_STATE_H
#define COMBAT_STATE_H

#include <engine/actor.h>
#include <engine/state.h>

class combat_state : public state {

private:

  actor *inst;
  actor *target;

  int    cur_tick;

public:

  /* I - inst and target */
  combat_state(actor *, actor *);

  state *transit(int);
  void enter();
  void update(int);
  void leave();
  void dump();

};
#endif
