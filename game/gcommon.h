/**
 * Common typedef, macro, constants
 *
 * Kai W(kkw)
 */
#ifndef GCOMMON_H
#define GCOMMON_H

#include <engine/jparser.h>
#include <string>
using namespace std;

namespace GAME {

const int TIME = 60 * 1000 * 1000;
const int FRAME = 16 * 1000; // 16ms, i.e. ~62fps

const int GAME_END = 0xdead;
const int GAME_OBS = 0x1003;
const int GAME_MOV = 0x1004;
const int GAME_CONT = 0x1005;

const int STATE_MOVE = 0x0001;
const int STATE_OBS = 0x0002;

const int ACT_STATE_PLY = 0x0001;
const int ACT_STATE_SLP = 0x0002;
const int ACT_STATE_DRI = 0x0003;
const int ACT_STATE_CBT = 0x0004;

const int ALE_LESSER = 0x0000;
const int ALE_NORMAL = 0x0001;
const int ALE_SUPERIOR = 0x0002;

const string EVENT_SPUSH = "event_spush";
const string EVENT_SPOP = "event_spop";
const string EVENT_COMBAT = "event_combat";
const string EVENT_INCOME = "event_income";
const string EVENT_SPAWN = "event_spawn";

} // namespace GAME
#endif
