#include <engine/NCUR.h>
#include <engine/actor_manager.h>
#include <engine/eventbus.h>
#include <engine/factory.h>
#include <engine/input_handler.h>
#include <engine/jobject.h>
#include <engine/logger.h>
#include <engine/smanager.h>
#include <engine/world_manager.h>
#include <fstream>
#include <game/game.h>
#include <game/gcommon.h>
#include <game/move_state.h>
#include <iostream>
#include <ncurses.h>

bool game::can_move() { return cache >= move_cost; }

bool game::can_spawn(string type) { return cache >= spawn_cost[type].jint; }

void game::init() {
    logger::log(logger::PLY_LOGG, "initializinging world");

    auto &config = m_actor.getConfig();
    for (const auto &[k, v] : config) {
        logger::log(logger::PLY_LOGG, "set_maker: %s as &X", k.c_str(),
                    &m_actor);
        factory::set_maker(k, &m_actor);
    }

    config = m_item.getConfig();
    for (const auto &[k, v] : config) {
        logger::log(logger::PLY_LOGG, "set_maker: %s as &X", k.c_str(),
                    &m_item);
        factory::set_maker(k, &m_item);
    }

    jparser jp;
    auto jlevel = jp.parse("game/level.json");

    cache = (*jlevel)["init_cache"].jint;
    spawn_cost = (*jlevel)["spawn_data"];
    move_cost = (*jlevel)["move_cost"].jint;

    world_manager::init_scene_graph((*jlevel)["width"].jint,
                                    (*jlevel)["height"].jint);
    world_manager::init_tiles((*jlevel)["map_data"]);
    world_manager::init_mobs((*jlevel)["mob_data"]);

    state_cur = make_shared<move_state>(this);
    state_cur->enter();

    h_game = game_handler(this);

    eventbus::add_listener(GAME::EVENT_SPUSH, &h_state);
    eventbus::add_listener(GAME::EVENT_SPOP, &h_state);
    eventbus::add_listener(GAME::EVENT_INCOME, &h_game);
    eventbus::add_listener(GAME::EVENT_SPAWN, &h_game);
}

int game::logic() {
    input_handler::update();

    if (input_handler::is_pressed(' ')) {
        state *new_state = state_cur->transit(' ');
        state_cur->leave();
        delete state_cur;
        state_cur = new_state;
        state_cur->enter();
    } else if (input_handler::is_pressed('q')) {
        return GAME::GAME_END;
    }

    state_cur->update(frame_total);

    eventbus::run();
    actor_manager::clear_deads();

    frame_total = (frame_total + GAME::FRAME) % GAME::TIME;
    usleep(GAME::FRAME);

    return GAME::GAME_CONT;
}

game::~game() {
    delete state_cur;
    actor_manager::clear();
    smanager::clear();
    NCUR::end();
    eventbus::shutdown();
}
