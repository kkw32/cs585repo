/**
 * Player actor
 *
 * Kai W(kkw)
 */
#ifndef PLAYER_H
#define PLAYER_H

#include <utils/garray.h>
#include <engine/actor.h>

class item;
class hall;

class player : public actor {

private:

  enum SLOT {
    WEAPON_SLOT=0,
    ARMOR_SLOT
  };

  /* Properties */
  int            gold;
  int            lv_cur;
  int            lv_max;
  int            exp_cur;
  int            exp_max;
  garray<int>    exp_range;
  garray<item *> potions;

  int            thirst_rate;
  int            thirst_max;
  int            drink_time;
  int            sleep_rate;
  int            sleep_max;
  int            sleep_time;

  item *         slots[10];
  hall *         home;

public:

  player(string);

  int get_num_potion()                  { return potions.size(); }
  int get_gold()                        { return gold; }
  int get_level_cur()                   { return lv_cur; }
  int get_level_max()                   { return lv_max; }
  int get_exp_cur()                     { return exp_cur; }
  int get_exp_max()                     { return exp_max; }

  int get_thirst_rate()                 { return thirst_rate; }
  int get_thirst_max()                  { return thirst_max; }
  int get_drink_time()                  { return drink_time; }
  int get_sleep_rate()                  { return sleep_rate; }
  int get_sleep_max()                   { return sleep_max; }
  int get_sleep_time()                  { return drink_time; }
  hall *get_home()                      { return home; }
  item *get_weapon()                    { return slots[WEAPON_SLOT]; }
  item *get_armor()                     { return slots[ARMOR_SLOT]; }

  void set_level_max(int l)             { lv_max=l; }
  void set_exp_max(int e)               { exp_max=e; }
  void set_exp_range(garray<int> range) { exp_range=range; exp_max=range[lv_cur]; }

  void set_thirst_rate(int tr)          { thirst_rate=tr; }
  void set_thirst_max(int tm)           { thirst_max=tm; }
  void set_drink_time(int dt)           { drink_time=dt; }
  void set_sleep_rate(int sr)           { sleep_rate=sr; }
  void set_sleep_max(int sm)            { sleep_max=sm; }
  void set_sleep_time(int st)           { sleep_time=st; }
  void set_home(hall *h)                { home=h; }

  void add_exp(int);
  void add_gold(int);
  void use_potion(int);
  /* O - 1 for a successful purchase, or 0 if can't buy */
  bool buy_ale();
  /* Upgrading logic */
  void upgrade_item();
  /* Set target at hall */
  void go_home();
  void go_back();
  /* Use potion */
  void use_potion();

  bool is_player();

  /**
   * I - slot number, item to equip
   */
  void equip(int, item *);
  /**
   * I - slot number
   */
  void unequip(int);

  ~player();

};
#endif
