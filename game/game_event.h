/**
 * In-game events
 * 
 * Kai W(kkw)
 */
#ifndef GAME_EVENT_H
#define GAME_EVENT_H

#include <engine/event.h>
#include <game/gcommon.h>

class game_event : public event {

private:

  int    param_int;
  string param_str;

public:

  game_event(string t): event(t) {}
  /* I - int parameter */
  game_event(string t, int p): event(t)    { param_int=p; }
  /* I - string parameter */
  game_event(string t, string p): event(t) { param_str=p; }

  int get_int()     { return param_int; }
  string get_str()  { return param_str; }

};
#endif
