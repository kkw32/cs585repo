#include <engine/item.h>
#include <engine/eventbus.h>
#include <engine/factory.h>
#include <engine/actor_manager.h>
#include <game/gcommon.h>
#include <game/game_event.h>
#include <game/hall.h>
#include <game/player.h>
#include <cstring>

player::player(string _n): actor(_n),
  gold(0),
  lv_cur(1),
  lv_max(1),
  exp_cur(0),
  exp_max(0),
  thirst_rate(0),
  thirst_max(0),
  drink_time(0),
  sleep_rate(0),
  sleep_max(0),
  sleep_time(0),
  home(0) {

  memset(slots, 0, sizeof slots);

}

void player::add_gold(int g) {
  gold+=g;
}

void player::add_exp(int e) {

  exp_cur+=e;

  while (exp_cur>=exp_max) {
    if (lv_cur<lv_max) {
      lv_cur++;
      exp_cur-=exp_max;
      exp_max=exp_range[lv_cur];
      dmg_min+=5;
      dmg_max+=5;
      health+=50;
      health_cur=health;
    } else {
      exp_cur=exp_max;
    }
  }

}

bool player::buy_ale() {
  item *ale=factory::make<item *>("ale");
  for (int level=home->get_level_ale(); level>=GAME::ALE_LESSER; level--) {
    int price=ale->get_price(level);
    if (gold>=price) {
      gold-=price;
      eventbus::post(new game_event(GAME::EVENT_INCOME, price*home->get_tax_rate()));
      delete ale;
      return 1;
    }
  }
  delete ale;
  return 0;
}

void player::upgrade_item() {

  item *weapon=factory::make<item *>("sword");
  int level=home->get_level_weapon();
  int price=weapon->get_price(level);

  if (gold>=price) {

    if (!slots[WEAPON_SLOT]) {
      equip(WEAPON_SLOT, weapon);
      dmg_bonus+=weapon->get_bonus();
      gold-=price;
    }
    else if (slots[WEAPON_SLOT]->get_level()<level) {
      weapon->set_level(level);
      dmg_bonus-=slots[WEAPON_SLOT]->get_bonus();
      unequip(WEAPON_SLOT);
      equip(WEAPON_SLOT, weapon);
      dmg_bonus+=weapon->get_bonus();
      gold-=price;
    }

  }

  item *armor=factory::make<item *>("chest armor");
  level=home->get_level_armor();
  price=armor->get_price(level);

  if (gold>=price) {

    if (!slots[ARMOR_SLOT]) {
      equip(ARMOR_SLOT, armor);
      def_bonus+=armor->get_bonus();
      gold-=price;
    }
    else if (slots[ARMOR_SLOT]->get_level()<level) {
      armor->set_level(level);
      def_bonus-=slots[ARMOR_SLOT]->get_bonus();
      unequip(ARMOR_SLOT);
      equip(ARMOR_SLOT, armor);
      def_bonus+=armor->get_bonus();
      gold-=price;
    }

  }

  item *potion=factory::make<item *>("potion");
  level=home->get_level_potion();
  price=potion->get_price(level);

  if (gold>=price) {
    potion->set_level(level);
    potions.push_back(potion);
    gold-=price;
  }

}

void player::go_home() {
  org_x=x;
  org_y=y;
  tar_x=home->get_x();
  tar_y=home->get_y();
}

void player::go_back() {
  actor_manager::update_actor(this, org_x, org_y);
}

void player::use_potion() {

  if (!potions.size() || health/health_cur<2) {
    return;
  }

  /* Find best fit potion */
  int use=potions.size()-1;
  int min_heal=potions[use]->get_bonus();
  for (int i=1; i<potions.size(); i++) {
    int heal=potions[i]->get_bonus();
    if (health_cur+heal<=health && heal<min_heal) {
      use=i;
    }
  }

  /* If not found, use the last one */
  potions.erase(use);
  health_cur+=min_heal;
  if (health_cur>=health) {
    health_cur=health;
  }

}

void player::equip(int slot, item *i) {

  if (slots[slot]) {
    return;
  }

  slots[slot]=i;

}

void player::unequip(int slot) {

  if (!slots[slot]) {
    return;
  }

  delete slots[slot];
  slots[slot]=0;

}

bool player::is_player() {
  return 1;
}

player::~player() {

  for (int i=0; i<potions.size(); i++) {
    delete potions[i];
  }

  for (int i=0; i<10; i++) {
    delete slots[i];
  }

}
