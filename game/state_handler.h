/**
 * State event handler
 *
 * Kai W(kkw)
 */
#ifndef STATE_HANDLER_H
#define STATE_HANDLER_H

#include <engine/handler.h>
#include <engine/state.h>
#include <engine/actor.h>
#include <game/gcommon.h>
#include <game/state_event.h>
#include <string> 
using namespace std;

class state_handler : public handler<state_event *> {

public:

  state_handler() {}

  void handle_event(state_event *e) {
 
    string type=e->get_type();
    state *to=e->get_state();
    actor *a=e->get_actor();

    if (type==GAME::EVENT_SPUSH) {
      a->cur_state()->leave();
      a->push_state(to);
      a->cur_state()->enter();
    } else if (type==GAME::EVENT_SPOP) {
      a->cur_state()->leave();
      a->pop_state();
    }

  }

};
#endif
