/**
 * Player state
 *
 * Kai W(kkw)
 */
#ifndef PLAYER_STATE_H
#define PLAYER_STATE_H

#include <engine/state.h>
#include <engine/actor.h>
#include <game/player.h>

class player_state : public state {

protected:

  player *inst;

  /**
   * Routing function pointer
   *
   * this function is used in all derived player_states, to route actor to
   * target
   */
  typedef void (player_state::*route_func)();
  route_func route;

  /* Tries attack the actor at coords */
  bool attack(int, int);
  void route_manual();
  void route_auto();

public:

  route_func get_route_auto() {
    return &player_state::route_auto;
  }

  route_func get_route_manual() {
    return &player_state::route_manual;
  }

  player_state(actor *p) {
    inst=static_cast<player *>(p);
    route=&player_state::route_manual;
  }

};
#endif
