/**
 * Mob actor
 *
 * Kai W(kkw)
 */
#ifndef MOB_H
#define MOB_H

#include <engine/actor.h>

class mob : public actor {

private:

  int loot_min;
  int loot_max;
  int exp;

public:

  mob(string);

  int get_loot_min()                 { return loot_min; }
  int get_loot_max()                 { return loot_max; }
  int get_exp()                      { return exp; }

  void set_loot_min(int lmin)        { loot_min=lmin; }
  void set_loot_max(int lmax)        { loot_max=lmax; }
  void set_exp(int e)                { exp=e; }

  bool is_player();

};
#endif
