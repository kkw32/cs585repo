#include <engine/eventbus.h>
#include <engine/window.h>
#include <engine/item.h>
#include <game/state_event.h>
#include <game/game_event.h>
#include <game/gcommon.h>
#include <game/rest_state.h>
#include <game/normal_state.h>

normal_state::normal_state(actor *p): player_state(p) {
  id=GAME::ACT_STATE_PLY;
  thirst=0;
  sleep=0;
  hunting=0;
}

state *normal_state::transit(int tevent) {
  return 0;
}

void normal_state::enter() {
  thirst=0;
  sleep=0;
  route=player_state::get_route_manual();
}

void normal_state::leave() {
}

void normal_state::update(int new_tick) {

  (this->*route)();

  sleep+=inst->get_sleep_rate();
  if (sleep>=inst->get_sleep_max()) {
    eventbus::post(
      new state_event(
        GAME::EVENT_SPUSH,
        new rest_state(inst, inst->get_sleep_rate(), inst->get_sleep_time()),
        inst
      )
    );
    sleep=0;
    inst->upgrade_item();
    return;
  }

  if (!hunting) {
    thirst+=inst->get_thirst_rate();
  }

  if (thirst>=inst->get_thirst_max()) {
    if (inst->buy_ale()) {
      hunting=0;
      route=player_state::get_route_manual();
      eventbus::post(
        new state_event(
          GAME::EVENT_SPUSH,
          new rest_state(inst, inst->get_thirst_rate(), inst->get_drink_time()),
          inst
        )
      );
      thirst=0;
    } else {
      hunting=1;
      route=player_state::get_route_auto();
    }
  }

}

void normal_state::dump() {
  mvwprintw(window::inf_win, 1, 30, "lv.%3d  %4d/%4d",
    inst->get_level_cur(), inst->get_exp_cur(), inst->get_exp_max());
  mvwprintw(window::inf_win, 1, 50, "HP      %4d/%4d", inst->get_health_cur(), inst->get_health());
  mvwprintw(window::inf_win, 2, 30, "gold    %4d", inst->get_gold());
  mvwprintw(window::inf_win, 2, 50, "DMG     %3d-%3d +%d", inst->get_dmg_min(), inst->get_dmg_max(), inst->get_dmg_bonus());
  mvwprintw(window::inf_win, 3, 30, "thirst  %4d/%4d", thirst, inst->get_thirst_max());
  mvwprintw(window::inf_win, 3, 50, "DEF     %3d-%3d +%d", inst->get_def_min(), inst->get_def_max(), inst->get_def_bonus());
  mvwprintw(window::inf_win, 4, 30, "sleep   %4d/%4d", sleep, inst->get_sleep_max());
  mvwprintw(window::inf_win, 4, 50, "potion   X%d", inst->get_num_potion());

  item *i;
  i=inst->get_weapon();
  i ?  mvwprintw(window::inf_win, 5, 30, "weapon  %s lv.%d", i->get_name().c_str(), i->get_level()) :
       mvwprintw(window::inf_win, 5, 30, "weapon  null");
  i=inst->get_armor();
  i ?  mvwprintw(window::inf_win, 5, 50, "armor   %s lv.%d", i->get_name().c_str(), i->get_level()) :
       mvwprintw(window::inf_win, 5, 50, "armor   null");
}
