/**
 * Game state: observer state
 *
 * Kai W(kkw)
 */
#ifndef OBS_STATE_H
#define OBS_STATE_H

#include <game/game_state.h>

class obs_state : public game_state {

private:

  /* Temporary stores the actor when it is chosen */
  actor *temp;
  actor *temp2;
  actor *crossing;
  /**
   * Chooses an actor (f key)
   * 
   * find_target - when f key is first pressed, enter finding state
   * set_target  - when f key is pressed again, set chosen actor's target
   */
  typedef void (obs_state::*find_func)();
  find_func find;
  void find_target();
  void set_target();

  void spawn_hall();
  void upgrade_ale();
  void upgrade_potion();
  void upgrade_weapon();
  void upgrade_armor();

  void recruit_dwarf();
  void recruit_wizard();

public:

  obs_state(game *);

  state *transit(int);
  void enter();
  void update(int);
  void leave();

};
#endif
