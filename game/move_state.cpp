#include <engine/smanager.h>
#include <engine/scene_graph.h>
#include <engine/window.h>
#include <engine/input_handler.h>
#include <engine/actor_manager.h>
#include <game/gcommon.h>
#include <game/game.h>
#include <game/obs_state.h>
#include <game/move_state.h>

move_state::move_state(game *g): game_state(g) {
  id=GAME::STATE_MOVE;
}

state *move_state::transit(int tevent) {
  if (tevent==' ') {
    return new obs_state(inst);
  }
  return 0;
}

void move_state::enter() {
  /* Draw camera coords on info window */
  window::hide_cur();

  wmove(window::inf_win, 1, 1);
  wclrtobot(window::inf_win);
  smanager::get_graph()->move_pos(0, 0);

  wrefresh(window::inf_win);

}

void move_state::update(int new_tick) {

  /* Handle input */
  if (input_handler::is_pressed('h')) {
    smanager::get_graph()->move_pos( 0, -1);
  } else if (input_handler::is_pressed('l')) {
    smanager::get_graph()->move_pos( 0,  1);
  } else if (input_handler::is_pressed('k')) {
    smanager::get_graph()->move_pos(-1,  0);
  } else if (input_handler::is_pressed('j')) {
    smanager::get_graph()->move_pos( 1,  0);
  }

  garray<actor *> ticks=actor_manager::all_ticks();
  for (int i=0; i<ticks.size(); i++) {
    ticks[i]->update(new_tick);
  }

  /* Draw cache */
  wmove(window::inf_win, 0, 1);
  wclrtoeol(window::inf_win);
  mvwprintw(window::inf_win, 0, 1, "%d", inst->get_cache());

  wrefresh(window::inf_win);

  smanager::render();

}

void move_state::leave() {
}
