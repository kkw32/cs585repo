/**
 * Game state: move state
 *
 * Kai W(kkw)
 */
#ifndef MOVE_STATE_H
#define MOVE_STATE_H

#include <game/game_state.h>

class move_state : public game_state {

public:

  move_state(game *);

  state *transit(int);
  void enter();
  void update(int);
  void leave();

};
#endif
