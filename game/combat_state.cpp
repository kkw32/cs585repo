#include <engine/eventbus.h>
#include <engine/actor_manager.h>
#include <engine/window.h>
#include <game/gcommon.h>
#include <game/player.h>
#include <game/mob.h>
#include <game/hall.h>
#include <game/state_event.h>
#include <game/game_event.h>
#include <game/combat_state.h>

combat_state::combat_state(actor *i, actor *t): inst(i), target(t) {
  id=GAME::ACT_STATE_CBT;
}

state *combat_state::transit(int tevent) {
  return 0;
}

void combat_state::enter() {
  cur_tick=-1;
}

void combat_state::update(int new_tick) {
  /**
   * TODO: To avoid is_player test and static_cast, we can split combat_state into
   * more *_combat_state
   */

  /* potion logic */
  if (inst->is_player()) {
    player *p=static_cast<player *>(inst);
    p->use_potion();
  }

  /* attack */
  int att_interval=inst->get_att_interval();
  if (new_tick<cur_tick) {
    cur_tick=-1;
  }

  if (cur_tick==-1 || (new_tick-cur_tick)>=att_interval) {

    cur_tick=new_tick;

    int dmg_min=inst->get_dmg_min();
    int dmg_max=inst->get_dmg_max();
    int dmg=rand()%(dmg_max-dmg_min)+dmg_min;
    dmg+=inst->get_dmg_bonus();

    int def_min=target->get_def_min();
    int def_max=target->get_def_max();
    int def=rand()%(def_max-def_min)+def_min;
    def+=target->get_def_bonus();

    dmg-=def;
    if (dmg<=0) {
      dmg=1;
    }

    target->take_dmg(dmg);
    if (target->is_dead()) {

      if (inst->is_player() && !target->is_player()) { // PvE, loot, remove target, and pop combat state

        player *p=static_cast<player *>(inst);
        mob *m=static_cast<mob *>(target);
        int minl=m->get_loot_min();
        int maxl=m->get_loot_max();
        p->add_gold(rand()%(maxl-minl)+minl);
        p->add_exp(m->get_exp());

        eventbus::post(new game_event(GAME::EVENT_SPAWN, m->get_name()));

      } else if (!inst->is_player() && target->is_player()) { // EvP, remove player
        player *p=static_cast<player *>(target);
        p->get_home()->release_dwarf();
      }

      inst->set_combat(0);
      eventbus::dispatch(new state_event(GAME::EVENT_SPOP, 0, inst));

    }

  }

}

void combat_state::leave() {
}

void combat_state::dump() {
  mvwprintw(window::inf_win, 1, 30, "target   : %s:%X", target->get_name().c_str(), target);
  mvwprintw(window::inf_win, 2, 30, "target HP: %d", target->get_health());
  mvwprintw(window::inf_win, 3, 30, "ATT INT  : %d", inst->get_att_interval());
}
