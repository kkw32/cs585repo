#include <engine/factory.h>
#include <engine/actor_manager.h>
#include <game/game.h>
#include <game/player.h>
#include <game/hall.h>

hall::hall(string _n): actor(_n),
  level_ale(0),
  level_potion(0),
  level_weapon(0),
  level_armor(0),
  level_ale_max(0),
  level_potion_max(0),
  level_weapon_max(0),
  level_armor_max(0),
  num_dwarf(0),
  num_wizard(0),
  dwarf_cost(0),
  apothecary_cost(0),
  blacksmith_cost(0),
  tax_rate(0),
  apothecary(0),
  blacksmith(0) {

  g=0;

}

void hall::upgrade_level_ale() {
  if (level_ale<level_ale_max &&
      g->get_cache()>=upgrade_range[level_ale]) {
    g->add_cache(-upgrade_range[level_ale]);
    level_ale++;
  }
}

void hall::upgrade_level_potion() {
  if (apothecary && level_potion<level_potion_max &&
      g->get_cache()>=upgrade_range[level_potion]) {
    g->add_cache(-upgrade_range[level_potion]);
    level_potion++;
  }
}

void hall::upgrade_level_weapon() {
  if (blacksmith && level_weapon<level_weapon_max &&
      g->get_cache()>=upgrade_range[level_weapon]) {
    g->add_cache(-upgrade_range[level_weapon]);
    level_weapon++;
  }
}

void hall::upgrade_level_armor() {
  if (blacksmith && level_armor<level_armor_max &&
      g->get_cache()>=upgrade_range[level_armor]) {
    g->add_cache(-upgrade_range[level_armor]);
    level_armor++;
  }
}

void hall::recruit_dwarf() {
  if (g->get_cache()>=dwarf_cost) {
    player* d=factory::make<player*>("dwarf");
    d->set_x(x);
    d->set_y(y);
    d->set_home(this);
    actor_manager::add_tick(d);
    g->add_cache(-dwarf_cost);
    num_dwarf++;
  }
}

void hall::hire_apothecary() {
  if (!apothecary && g->get_cache()>=apothecary_cost) {
    g->add_cache(-apothecary_cost);
    apothecary=1;
  }
}

void hall::hire_blacksmith() {
  if (!blacksmith && g->get_cache()>=blacksmith_cost) {
    g->add_cache(-blacksmith_cost);
    blacksmith=1;
  }
}

bool hall::is_building() {
  return 1;
}
