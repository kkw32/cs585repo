/**
 * Player state: sleep state
 *
 * Kai W(kkw)
 */
#ifndef REST_STATE_H
#define REST_STATE_H

#include <game/player_state.h>

class rest_state : public player_state {

private:

  int rest_time;
  int rest_rate;
  int rest_time_max;

public:

  /**
   * I - rest rate, max rest time
   */
  rest_state(actor *, int, int);
 
  state *transit(int);
  void enter();
  void update(int);
  void leave();
  void dump();

};
#endif
