CXX      =g++
CXXFLAGS =-g -Wall -std=c++2a
LIBS     =-lncurses
VPATH    =utils:engine:game:test
INCLUDES =-I"./"

UTEST_OBJ=utiltest.o logger.o
GTEST_OBJ=gametest.o logger.o smanager.o fixed_grid.o game.o eventbus.o \
          jparser.o window.o NCUR.o input_handler.o move_state.o obs_state.o \
          player_state.o actor.o jobject.o normal_state.o rest_state.o \
          game_handler.o actor_manager.o world_manager.o player.o mob.o \
          actor_maker.o factory.o combat_state.o hall.o building_state.o \
          item_maker.o
JTEST_OBJ=jsontest.o logger.o jparser.o jobject.o

gtest: $(GTEST_OBJ)
	$(CXX) $^ -o $@ $(LIBS)

jtest: $(JTEST_OBJ)
	$(CXX) $^ -o $@ $(LIBS)

%.o: %.cpp
	$(CXX) -c $(CXXFLAGS) $< -o $@ $(INCLUDES)

cl:
	rm *.o utest gtest jtest log
