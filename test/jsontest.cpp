#include <cstdio>
#include <engine/jobject.h>
#include <engine/jparser.h>
#include <fstream>
#include <iostream>
#include <string>
using namespace std;

shared_ptr<jobject> get() {
    jparser jp;
    return jp.parse("test/data");
}

int main(void) {
    auto jo = get();

    printf("html_attributions->size=%d\n", (*jo)["html_attributions"]->size());
    printf("status=%s\n", (*jo)["status"]->jstring.c_str());

    auto &jr = *(*jo)["result"];

    printf("address_components:\n");
    for (int i = 0; i < jr["address_components"]->size(); i++) {
        auto &com = *(*jr["address_components"])[i];
        printf("  long_name=%s\n", com["long_name"]->jstring.c_str());
        printf("  short_name=%s\n", com["short_name"]->jstring.c_str());
        printf("  types: ");
        for (int j = 0; j < com["types"]->size(); j++) {
            printf("%s ", (*com["types"])[j]->jstring.c_str());
        }
        puts("\n");
    }

    printf("events:\n");
    for (int i = 0; i < jr["events"]->size(); i++) {
        auto &e = *(*jr["events"])[i];
        printf("  event_id=%s\n", e["event_id"]->jstring.c_str());
        printf("  start_time=%lld\n", e["start_time"]->jint);
        printf("  summary=%s\n", e["summary"]->jstring.c_str());
        printf("  url=%s\n", e["url"]->jstring.c_str());
    }

    printf("reviews:\n");
    for (int i = 0; i < jr["reviews"]->size(); i++) {
        auto &r = *(*jr["reviews"])[i];
        printf("  aspects:\n");
        for (int j = 0; j < r["aspects"]->size(); j++) {
            auto &a = *(*r["aspects"])[j];
            printf("    rating=%lld\n", a["rating"]->jint);
            printf("    type=%s\n", a["type"]->jstring.c_str());
        }
        printf("  author_name=%s\n", r["author_name"]->jstring.c_str());
        printf("  author_url=%s\n", r["author_url"]->jstring.c_str());
        printf("  text=%s\n", r["text"]->jstring.c_str());
        printf("  time=%lld\n", r["time"]->jint);
    }
}
