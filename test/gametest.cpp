#include <cstdlib>
#include <engine/NCUR.h>
#include <engine/actor_manager.h>
#include <engine/logger.h>
#include <engine/window.h>
#include <engine/world_manager.h>
#include <game/game.h>
#include <game/game_state.h>
#include <game/gcommon.h>
#include <unistd.h>
using namespace std;

int main(void) {
    srand(time(0));

    logger::init();

    logger::log(logger::PLY_LOGG, "game started");
    system("clear");

    NCUR::init();
    window::init();
    world_manager::init();

    game g;
    g.init();

    int state = g.logic();
    while (state != GAME::GAME_END) {
        state = g.logic();
    }
}
